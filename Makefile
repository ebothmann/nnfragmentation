.PHONY: init small-dataset medium-dataset large-dataset

init:
	bin/setup

partonargs="FRAGMENTATION=None MI_HANDLER=None BEAM_REMNANTS=0"

small-dataset:
	Sherpa -e 100 ${partonargs} EVENT_OUTPUT=HepMC_GenEvent[parton-level.small]
	Sherpa -e 100 EVENT_OUTPUT=HepMC_Short[hadron-level.small]
	for f in *.small.hepmc* ; do mv $$f $$f.gz ; done

medium-dataset:
	Sherpa -e 10k ${partonargs} EVENT_OUTPUT=HepMC_GenEvent[parton-level.medium]
	Sherpa -e 10k EVENT_OUTPUT=HepMC_Short[hadron-level.medium]
	for f in *.medium.hepmc* ; do mv $$f $$f.gz ; done

large-dataset:
	Sherpa -e 1M ${partonargs} EVENT_OUTPUT=HepMC_GenEvent[parton-level.large]
	Sherpa -e 1M EVENT_OUTPUT=HepMC_Short[hadron-level.large]
	for f in *.large.hepmc* ; do mv $$f $$f.gz ; done
