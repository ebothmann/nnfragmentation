# Getting started using

```bash
git clone git@gitlab.com:ebothmann/nnfragmentation.git
cd nnfragmentation
make init       # install dependencies if needed
make small-dataset
gunzip *.gz
vim -O *.hepmc  # compare parton-level and hadron-level output
```

Note that `make init` will install [Sherpa](https://sherpa.hepforge.org) and
its dependencies using the [Homebrew package manager](https://brew.sh) for
macOS, if needed. This is done by downloading and executing the Homebrew
installation script. This is a potential security risk and you might want to
install it more manually, see the [Homebrew site](https://brew.sh) for more
details.

# References

- [Monte-Carlo particle numbering scheme](https://link.springer.com/article/10.1007%2FBF02683426)

- [HepMC manual](http://lcgapp.cern.ch/project/simu/HepMC/206/HepMC2_user_manual.pdf)  
  this includes detailed information about the event output format
